#!/usr/bin/env python2

import tensorflow as tf
import numpy as np
import os
import random
import datetime
import optparse

PATH = "/data/spacenet2/"
IMG_PATH = os.path.join(PATH, "images")
HEATMAP_PATH = os.path.join(PATH, "heatmaps")
CHCKPNT_DIR = "checkpoint"
# TODO change name
CHCKPNT_NAME = "checkpoint/mnist_model.chkpnt"
CHCKPNT_NAME_FOR_CHECK = CHCKPNT_NAME + ".index"
LOGS_PATH = "logs"

TEST_FRACTION = 0.1
NUM_CHANNELS = 3
ORIG_IMAGE_HEIGHT = 650
ORIG_IMAGE_WIDTH = 650
LOSS_LIMIT = 0.15
BATCHES_PER_MEAN = 1
NUM_THREADS_PER_BATCH = 10
MINI_PER_BATCH = 400
MINIBATCH_SIZE = 8
TEST_MINIBATCH_SIZE = 8
QUEUE_CAPACITY = 32
HEIGHT = 352
WIDTH = 352
LEARNING_RATE = 0.008
LEARNING_DECAY = 0.75
epsilon = 1e-16

def partition_data(data):
    test_size = int(len(data) * TEST_FRACTION)
    partitions = [0] * len(data)
    partitions[:test_size] = [1] * test_size
    # we want to create data partition deterministically
    # also I don't want to make my whole program deterministic
    my_rand = random.Random()
    my_rand.seed(891018608)
    my_rand.shuffle(partitions)
    data = zip(data, partitions)
    test_data = map(lambda y: y[0], filter(lambda x: x[1] == 1, data))
    train_data = map(lambda y: y[0], filter(lambda x: x[1] == 0, data))
    return train_data, test_data

def split_into_tf_imgs_and_heatmaps(data):
    img_data = []
    hm_data = []
    for f in data:
        img_data.append(os.path.join(IMG_PATH, f))
        hm_data.append(os.path.join(HEATMAP_PATH, f))
    return (tf.constant(img_data), tf.constant(hm_data))

def convert_data_to_tensors(data):
    return tf.constant(data)

def create_queue_for_dataset(data):
    data = split_into_tf_imgs_and_heatmaps(data)
    data_input_queue = tf.train.slice_input_producer(
        [data[0], data[1]], shuffle=True
    )
    return data_input_queue

def get_tf_rand():
    p_val = tf.random_uniform(shape=[], minval=0., maxval=1., dtype=tf.float32)
    return tf.less(p_val, 0.5)

def prepare_file(file_path, random_tensors=[]):
    file_content = tf.read_file(file_path)
    img = tf.image.decode_jpeg(file_content, channels=NUM_CHANNELS)
    if random_tensors:
        img = tf.cond(random_tensors[0], lambda: img, lambda: tf.image.flip_up_down(img))
        img = tf.cond(random_tensors[1], lambda: img, lambda: tf.image.rot90(img, k=1))
        img = tf.cond(random_tensors[2], lambda: img, lambda: tf.image.rot90(img, k=2))
    return tf.image.resize_images(img, [HEIGHT, WIDTH]) / 256

class Trainer(object):
    def __init__(self, options):
        self.prepare_data()
        self.create_models()
        self.should_generate_images = options.images
        self.img_batch_num = options.img_batch_num
        self.test_only = options.test_only

    def reset_layer_cnt(self):
        self._layer_cnt = 0;

    def get_layer_cnt(self):
        res = self._layer_cnt
        self._layer_cnt += 1
        return res

    def _add_conv_with_bn(self, signal, channels, kernel_size, strides, reuse, training,
                         transpose, stddev=0.5, use_activation=True):
        ln = self.get_layer_cnt()
        name = 'conv' + ("_tr" if transpose else "") + str(ln)
        fun = tf.layers.conv2d_transpose if transpose else tf.layers.conv2d
        kernel_initializer=tf.truncated_normal_initializer(stddev=stddev)
        signal = fun(signal, channels, kernel_size, use_bias=True,
                     kernel_initializer=kernel_initializer, padding='same',
                     name=name, reuse=reuse, strides=strides)
        name = 'bn' + str(ln)
        signal = tf.layers.batch_normalization(signal, axis=3, training=training,
                                               reuse=reuse, name=name)
        if use_activation:
            signal = tf.nn.relu(signal)
        return signal

    def add_conv_with_bn(self, signal, channels, kernel_size, reuse,
                         training, stddev=0.5, strides=1, use_activation=True):
        return self._add_conv_with_bn(signal, channels, kernel_size, strides,
                                      reuse, training, False, stddev=stddev,
                                      use_activation=use_activation)

    def add_conv_tr_with_bn(self, signal, channels, kernel_size, strides, reuse,
                            training, stddev=0.5):
        return self._add_conv_with_bn(signal, channels, kernel_size, strides,
                                      reuse, training, True, stddev=stddev)

    def add_max_pool(self, signal, pool_size, strides):
        ln = self.get_layer_cnt()
        name = 'max_pool' + str(ln)
        return tf.layers.max_pooling2d(signal, pool_size, strides, padding='same',
                                       name=name)

    # signal_2 can be bigger and must be cropped
    def crop_and_concat(self, signal1, signal2):
        shape1 = signal1.shape.as_list()
        shape2 = signal2.shape.as_list()
        # currently we want to avoid cropping
        assert shape1 == shape2
        offsets = [0, (shape2[1] - shape1[1]) // 2, (shape2[2] - shape1[2]) // 2, 0]
        size = [-1, shape1[1], shape1[2], -1]
        signal2_crop = tf.slice(signal2, offsets, size)
        return tf.concat([signal1, signal2_crop], 3)

    def create_graph(self, signal, training, reuse=None):
        self.reset_layer_cnt()

        channels = [32, 64, 128, 256, 512]
        steps = len(channels) - 1
        cached = []
        conv_size = 3

        for i in xrange(steps):
            signal = self.add_conv_with_bn(signal, channels[i], conv_size, reuse, training)
            signal = self.add_conv_with_bn(signal, channels[i], conv_size, reuse, training)
            cached.append(signal)
            signal = self.add_max_pool(signal, 2, 2)

        signal = self.add_conv_with_bn(signal, channels[steps], conv_size, reuse, training)
        signal = self.add_conv_with_bn(signal, channels[steps], conv_size, reuse, training)
        signal = self.add_conv_tr_with_bn(signal, channels[steps - 1], 2, 2, reuse, training)

        for i in xrange(steps - 1, -1, -1):
            signal = self.crop_and_concat(cached[i], signal)
            signal = self.add_conv_with_bn(signal, channels[i], conv_size, reuse, training)
            signal = self.add_conv_with_bn(signal, channels[i], conv_size, reuse, training)
            if i > 0:
                signal = self.add_conv_tr_with_bn(signal, channels[i - 1], 2, 2, reuse, training)

        signal = self.add_conv_with_bn(signal, 3, 1, reuse, training, use_activation=False)
        return signal

    def prepare_data(self):
        img_names = [f for f in os.listdir(IMG_PATH)
                     if os.path.isfile(os.path.join(IMG_PATH, f))]

        self.train_data, self.test_data = partition_data(img_names)

        self.train_input_queue = create_queue_for_dataset(self.train_data)
        self.test_input_queue = create_queue_for_dataset(self.test_data)

        self.train_batch = self.create_batches_from_queue(
            self.train_input_queue, random_flips=True)
        self.test_batch = self.create_batches_from_queue(self.test_input_queue)

    def create_batches_from_queue(self, input_queue, random_flips=False):
        random_tensors = []
        if random_flips:
            random_tensors = [get_tf_rand(), get_tf_rand(), get_tf_rand()]
        img = prepare_file(input_queue[0], random_tensors=random_tensors)
        hm = prepare_file(input_queue[1], random_tensors=random_tensors)
        to_batch = []
        if random_flips:
            to_batch = [img, hm]
            bsize = MINIBATCH_SIZE
        else:
            img_shape = [HEIGHT, WIDTH, NUM_CHANNELS]
            to_batch = [img, tf.image.flip_up_down(img),
                        tf.image.flip_left_right(img),
                        tf.image.rot90(img, k=2),
                        hm]
            self.valid_transf_num = len(to_batch) - 1
            to_batch[3].set_shape(img_shape)
            bsize = TEST_MINIBATCH_SIZE
        return tf.train.batch(to_batch, batch_size=bsize,
                              num_threads=NUM_THREADS_PER_BATCH,
                              capacity=QUEUE_CAPACITY)

    def calc_loss(self, signal, labels, with_activation=True):
        signal = tf.reshape(signal, [-1, 3])
        labels = tf.reshape(labels, [-1, 3])
        if with_activation:
            losses = tf.nn.softmax_cross_entropy_with_logits(logits=signal, labels=labels)
        else:
            losses = -tf.reduce_sum(labels * tf.log(tf.clip_by_value(signal, epsilon, 1)),
                                    axis=1)
        return tf.reduce_mean(losses)

    def create_models(self):
        global_step = tf.Variable(0, trainable=False);
        batches_per_epoch = len(self.train_data) / MINIBATCH_SIZE
        learning_rate = tf.train.exponential_decay(
            LEARNING_RATE, global_step, batches_per_epoch, LEARNING_DECAY);
        self.training = tf.placeholder(tf.bool)
        self.train_signal = self.create_graph(self.train_batch[0], self.training)
        self.train_loss = self.calc_loss(self.train_signal, self.train_batch[1])
        extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(extra_update_ops):
            self.train = tf.train.AdamOptimizer(learning_rate).minimize(
                self.train_loss, global_step=global_step)

        num = self.valid_transf_num
        self.test_input = tf.concat(self.test_batch[:num], 0)
        self.test_signal = self.create_graph(self.test_input, self.training, reuse=True)
        img, img_u_d, img_l_r, img_both = tf.split(self.test_signal, num, axis=0)
        self.test_pred = tf.nn.softmax(img)
        img1 = tf.map_fn(lambda im: tf.image.flip_up_down(im), img_u_d)
        img2 = tf.map_fn(lambda im: tf.image.flip_left_right(im), img_l_r)
        img3 = tf.map_fn(lambda im: tf.image.rot90(im, k=2), img_both)
        avg_pred = img / num + img1 / num + img2 / num + img3 / num
        self.soft_test = tf.nn.softmax(avg_pred)
        self.test_loss = self.calc_loss(avg_pred, self.test_batch[num])

    def test_model(self, sess, is_training):
        losses = []
        for _ in xrange(len(self.test_data) / TEST_MINIBATCH_SIZE):
            losses.append(sess.run([self.test_loss],
                                   feed_dict={self.training: is_training}))
        return np.mean(losses)

    def get_time_str(self):
        return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    def display_data(self, sess, images, heatmaps, avg_preds, preds):
        summaries_op = []
        idx = 0
        for tup in zip(images, heatmaps, avg_preds, preds):
            summaries_op.append(
                tf.summary.image("Example {}".format(idx), np.array(list(tup)),
                                 max_outputs=4))
            idx += 1
        summaries = sess.run(summaries_op)
        for summ in summaries:
            self.writer.add_summary(summ)

    def run_training(self, sess):
        losses = []
        mean_loss = 10
        val_loss = 10
        batch_idx = 0
        minibatch_idx = 0
        summary_op = tf.summary.merge_all()
        while mean_loss >= LOSS_LIMIT or val_loss >= LOSS_LIMIT:
            for _ in xrange(MINI_PER_BATCH):
                loss, _, summary = sess.run(
                    [self.train_loss, self.train, summary_op],
                    feed_dict={self.training: True})
                losses.append(loss)
                self.writer.add_summary(summary, minibatch_idx)
                minibatch_idx += 1
            losses = losses[-MINI_PER_BATCH * BATCHES_PER_MEAN:]
            mean_loss = np.mean(losses, axis=0)
            epoch_idx = batch_idx * MINI_PER_BATCH * MINIBATCH_SIZE / len(self.train_data)
            print('Time: {time}, epoch {epoch_idx}: mean_loss {mean_loss}, last_loss {loss}'.format(
                time=self.get_time_str(), epoch_idx=epoch_idx, loss=losses[-1],
                mean_loss=mean_loss)
            )
            val_loss = self.test_model(sess, False)
            print('Time: {time}, epoch {epoch_idx}: validation_loss {loss}'.format(
                time=self.get_time_str(), epoch_idx=epoch_idx,
                loss=val_loss)
            )
            self.saver.save(sess, CHCKPNT_NAME)
            batch_idx += 1

    def generate_images(self, sess):
        for _ in xrange(self.img_batch_num):
            img, hm, res, pred = sess.run(
                [self.test_batch[0], self.test_batch[self.valid_transf_num],
                 self.soft_test, self.test_pred],
                feed_dict={self.training: False})
            self.display_data(sess, img, hm, res, pred)

    def run(self):
        with tf.Session() as sess:
            self.saver = tf.train.Saver()
            if not os.path.exists(CHCKPNT_DIR):
                os.makedirs(CHCKPNT_DIR)
            if os.path.isfile(CHCKPNT_NAME_FOR_CHECK):
                print("Restoring last session")
                self.saver.restore(sess, CHCKPNT_NAME)
            else:
                sess.run(tf.global_variables_initializer())
            self.writer = tf.summary.FileWriter(LOGS_PATH, graph=tf.get_default_graph())
            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(coord=coord)

            if self.test_only:
                print('Validation loss {loss}'.format(
                    loss=self.test_model(sess, False)))
            else:
                if self.should_generate_images:
                    self.generate_images(sess)
                else:
                    self.run_training(sess)

            coord.request_stop()
            coord.join(threads)

parser = optparse.OptionParser()
parser.add_option("-i", "--images", action="store_true", dest="images",
                  default=False, help="generate images")
parser.add_option("-b", "--gen-img-batch-num", dest="img_batch_num",
                  type="int", default=1)
parser.add_option("-t", "--test-only", action="store_true", dest="test_only",
                  default=False, help="test-only mode")
(options, _) = parser.parse_args()

trainer = Trainer(options)
trainer.run()
